(ns quils.trisub.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def num-frames 100)
(def render (atom false))

(defn setup []
  (q/frame-rate 1)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn state->frame [state]
  (mod (:iteration state) num-frames))

(defn triangle-center [[[x0 y0] [x1 y1] [x2 y2]]]
  [(/ (+ x0 x1 x2) 3)
   (/ (+ y0 y1 y2) 3)])

(defn triangle-sides [triangle]
  (->> triangle
       cycle
       (partition 2 1)
       (take 3)))

(defn subdivide-once [triangle]
  (let [center (triangle-center triangle)]
    (->> triangle
         triangle-sides
         (map (partial cons center)))))

(defn subdivide [depth triangle]
  (sequence
   (apply comp (repeat depth (mapcat subdivide-once)))
   [triangle]))

(defn draw-triangle [triangle]
  (doseq [[[x0 y0] [x1 y1]] (triangle-sides triangle)]
    (q/line (+ x0 2) y0 x1 y1)))

(defn draw-circles [triangle radius]
  (let [[x y] (triangle-center triangle)]
    (q/ellipse x y radius radius)))

(defn new-triangle [radius]
  (into []
        (comp
         (map (partial + 3/12))
         (map (comp (partial map (partial * radius))
                    (juxt q/cos q/sin)
                    (partial * u/TAU))))
        [0 1/3 2/3]))

(defn draw-state [state]
  (q/background 200)
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/stroke 0 30)
  (q/no-fill)
  (q/stroke-weight 0.12)
  (q/translate 0 -1)

  (let [depth 7
        radius 10
        circle-radius 80]
    (doseq [t (subdivide depth (new-triangle radius))]
      (draw-circles t circle-radius)))

  (q/translate -1 0)

  (let [depth 8
        radius 30]
    (doseq [t (subdivide depth (new-triangle radius))]
      (draw-triangle t)))

  (when @render
    (q/save "out/trisub.png")
    (reset! render false)))
