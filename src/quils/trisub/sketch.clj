(ns quils.trisub.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.trisub.core :as core]))

(q/defsketch trisub
  :title "trisub"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
