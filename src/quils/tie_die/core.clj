(ns quils.tie-die.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def num-frames 200)
(def random-seed 3)
(def render false)

(defn setup []
  (q/frame-rate 30)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn state->frame [state]
  (mod (:iteration state) num-frames))

(defn cut [x]
  (- x (/ x u/PHI)))

(defn draw-rings [state]
  (q/no-stroke)
  (q/random-seed random-seed)
  (let [frame (state->frame state)
        iterations (* 800 (/ frame num-frames))]
    (u/with-matrix
      (doseq [i (range 1 iterations)]
        (let [size (* 10 (/ (- iterations i) iterations))
              scale (partial * 0.1 size)
              height (scale (q/random 25 50))
              width (scale (q/random 1 5))
              hue (mod (* i (/ 255 u/PHI)) 255)]
          (q/fill hue 255 255)
          (q/rotate (+ (* 1.7e-3 u/TAU) (cut u/TAU)))
          (q/rect (/ width -2) 0 #_ (/ height -2) width height)))))
  (q/random-seed (inc random-seed))
  (let [frame (- (state->frame state) (* 3/5 num-frames))
        iterations (* 800 (/ frame (* 3/5 num-frames)))]
    (when (> frame 0)
      (doseq [i (range 1 iterations)]
        (let [size (* 10 (/ (- iterations i) iterations))
              scale (partial * 0.1 size)
              height (scale (* 4 (q/random 25 50)))
              width (scale (q/random 2 3))]
          (q/fill 0 0 0)
          (q/rotate (+ (* 1.7e-3 u/TAU) (cut u/TAU)))
          (q/rect (/ width -2) 0 #_ (/ height -2) width height))))))

(defn draw-state [state]
  (q/background 0 0 0)
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor)
    (q/stroke-weight 0.5))
  (draw-rings state)
  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/tie-die/frame-%03d.tif" iteration))
        (q/exit)))))
