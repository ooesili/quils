(ns quils.tie-die.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.tie-die.core :as core]))

(q/defsketch tie-die
  :title "Tie-Die"
  :size [600 600]
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
