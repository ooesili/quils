(ns quils.prism-planet.core
  (:require [quil.core :as q]
            [quils.util :as u]
            [clojure.test :refer [is]]))

(def render (atom false))
(def num-colors 4)

(defn setup
  []
  (q/frame-rate 1)
  (q/color-mode :rgb)
  {})

(def update-state identity)

(defn draw-light [camera light]
  (q/blend-mode :add)
  (q/stroke-weight 0.35)
  (q/stroke 0)
  (let [{:keys [color radius position]} light
        [x y] (u/camera-project camera position)]
    (apply q/fill color)
    (u/circle x y (u/camera-projected-size camera position radius))))


(defn draw-lights [camera elements]
  (let [{[cx cy cz] :position} camera]
    (->> (flatten elements)
         (sort-by (fn [{[x y z] :position}]
                    (- (q/dist x y z cx cy cz))))
         (filter (fn [{:keys [position]}]
                   (let [[x y] (u/camera-project camera position)
                         x-size 45
                         y-size 45]
                     (and (< (- x-size) x x-size)
                          (< (- y-size) y y-size)))))
         (map (partial draw-light camera))
         doall)))

(defn draw-state [state]
  (q/random-seed 0)

  (q/background 12)
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (let [camera-position [0 -25 4.7]
        camera-angle [1.1 -0.4 0]
        camera-zoom 52
        camera (u/new-camera camera-position
                             camera-angle
                             camera-zoom)]

    (draw-lights
     camera
     (let [num-dots 4998]
       (for [i (range num-dots)]
         (let [lat (q/map-range i 0 num-dots 0 (* 1/2 u/TAU))
               lon (q/map-range i 0 num-dots 0 (* 124 u/TAU))]
           (for [[color-index color] (map vector (range) (u/split-colors num-colors))]
             (let [orbit (q/map-range color-index 0 num-colors 32 35)]
               {:type :light
                :position (->> [0 -0.1 orbit]
                               (u/rotate-3d [lat 0 0])
                               (u/rotate-3d [0 0 lon])
                               (u/rotate-3d [0 (* -1/3 u/TAU) 0]))
                :radius 0.2
                :color color})))))))

  (when @render
    (q/save "out/prism-planet.png")
    (reset! render false)))
