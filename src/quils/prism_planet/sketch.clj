(ns quils.prism-planet.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.prism-planet.core :as core]))

(q/defsketch prism-planet
  :title "prism-planet"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state

  :features []
  :middleware [m/fun-mode])
