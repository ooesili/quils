(ns quils.growing.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def decay-rate 0.99)
(def lifespan 150)
(def chlid-ratio 0.8)
(def split-velocity 0.3)

(def white [0 255])

(defn cut-color [[low high] child num-children]
  (let [span (/ (- high low) num-children)
        new-low (+ low (* span child))
        new-high (+ new-low span)]
    [new-low new-high]))

(defn color->hue [[low high]]
  (/ (+ low high) 2))

(def initial-state
  {:particles [{:pos [0 0]
                :velocity [0 0]
                :size 5
                :color white
                :age lifespan}]})

(defn setup []
  (q/frame-rate 15)
  (q/color-mode :hsb)
  (q/background 0 0 0)
  initial-state)

(defn particle-size [particle]
  (let [{:keys [age size]} particle]
    (* size (q/pow decay-rate age))))

(defn rotate-vector [[x y] angle]
  (let [sin-angle (q/sin angle)
        cos-angle (q/cos angle)]
    [(- cos-angle sin-angle)
     (+ sin-angle cos-angle)]))

(defn update-particle [particle]
  (if (< (:age particle) lifespan)
    [(-> particle
         (update :age inc)
         (update :pos (partial map + (:velocity particle))))]
    (let [num-children (q/round (q/random 1 4))]
      (for [child (range num-children)]
        (let [angle (+ (* u/TAU (/ child num-children))
                       (* u/TAU 1/4 (/ 1 num-children) (q/random-gaussian)))
              velocity [(* split-velocity (q/cos angle))
                        (* split-velocity (q/sin angle))]]
          {:pos (:pos particle)
           :velocity velocity
           :size (* chlid-ratio (:size particle))
           :color (cut-color (:color particle) child num-children)
           :age (q/random 0 lifespan)})))))

(defn update-state [state]
  (-> state
      (update :particles (partial mapcat update-particle))))

(defn draw-particles [state]
  (q/no-stroke)
  (doseq [particle (:particles state)]
    (let [[x y] (:pos particle)
          size (particle-size particle)]
      (q/fill (color->hue (:color particle)) 255 255 16)
      (q/ellipse x y size size))))

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (draw-particles state))
