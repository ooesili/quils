(ns quils.growing.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.growing.core :as core]))

(q/defsketch growing
  :title "growing"
  :size [800 800]
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
