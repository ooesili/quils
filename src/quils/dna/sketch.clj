(ns quils.dna.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.dna.core :as core]))

(q/defsketch dna
  :title "dna"
  :size [680 680]
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :renderer :p3d
  :middleware [m/fun-mode])
