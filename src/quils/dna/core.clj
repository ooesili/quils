(ns quils.dna.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def num-frames 100)
(defonce render false)

(defn setup []
  (q/frame-rate 10)
  (q/stroke-cap :square)
  (q/stroke-join :miter)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn state->frame [state]
  (mod (:iteration state) num-frames))

(defn coil-center [state]
  (let [x (-> (state->frame state)
              (+ 10)
              (q/map-range 0 num-frames 25 75))
        y x]
    [x y]))

(defn draw-dna [state]
  (q/fill 0)
  (q/stroke 255 255 255)
  (let [[x y] (coil-center state)]
    (q/ellipse x y 10 10)))

(defn draw-state [state]
  (q/stroke-cap :square)
  (q/background 0 0 0)
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))
  (draw-dna state)
  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/dna/frame-%03d.tif" iteration))
        (q/exit)))))
