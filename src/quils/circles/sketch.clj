(ns quils.circles.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.circles.core :as core]))

(q/defsketch circles
  :title "Circles"
  :size [1920 1080]
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
