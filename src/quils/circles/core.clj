(ns quils.circles.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def num-layers 5)
(def num-frames 600)
(def render false)

(defn setup []
  (q/frame-rate 30)
  (q/color-mode :hsb)
  (q/no-fill)
  (q/stroke-weight 0.5)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn state->iteration [state]
  (mod (:iteration state) num-frames))

(defn state->scale-factor [state]
  (-> (state->iteration state)
      (/ num-frames)
      (* u/TAU)
      (+ q/PI)
      q/cos
      (+ 0.5)))

(def path-layers
  (for [len (range num-layers)]
    (cons
      (repeat len :u)
      (for [i (range 1 len)]
        (concat
          (repeat (- len i) :u)
          [:r]
          (repeat i :u))))))

(defn draw-circles [props depth]
  (let [{:keys [angles circles main-diameter radius]} props
        indexed-layers (map-indexed vector path-layers)]
    (doseq [[size hue] circles]
      (let [diameter (* main-diameter size)]
        (doseq [[layer-index layer] indexed-layers
              path layer]
          (q/stroke hue 255 255 (* (- num-layers layer-index) (/ 255 num-layers)))
          (doseq [angle angles]
            (u/with-matrix
              (q/rotate angle)
              (doseq [move path]
                (case move
                  :u (q/translate 0 radius)
                  :r (q/rotate (/ u/TAU 6))))
                (q/ellipse 0 0 diameter diameter))))))))

(let [radius 10
      main-diameter (* 2 radius)
      angle-step (/ u/TAU 6)
      angles (map (partial * angle-step) (range 6))]
  (defn frame-props [state]
    (let [hues (for [hue [0 32 64 96]]
                 (let [hue-step (/ 255 num-frames)]
                   (mod (+ hue (* (state->iteration state) hue-step)) 255)))
          size-step (state->scale-factor state)
          sizes (iterate (partial + size-step) 1)]
      {:circles (map vector sizes hues)
       :angles angles
       :main-diameter main-diameter
       :radius radius})))

(defn draw-state [state]
  (q/background 0 0 0)
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))
  (draw-circles (frame-props state) num-layers)
  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/circles/frame-%03d.tif" iteration))
        (q/exit)))))
