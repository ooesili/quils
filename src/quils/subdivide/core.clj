(ns quils.subdivide.core
  (:require [quil.core :as q]))

(def max-depth 19)
(defonce render (atom false))

(defn setup []
  (q/color-mode :hsb))

(defn subdivide [depth rect]
  (when (< depth max-depth)
    (let [[x y w h] rect
          depth (inc depth)]
      (q/fill 0 192 255 24)
      (q/rect x y (/ h 2) (/ h 2))
      (if (<= w h)
        (do
          (subdivide depth [x y w (/ h 2)])
          (subdivide depth [x (+ y (/ h 2)) w (/ h 2)]))
        (do
          (subdivide depth [x y (/ w 2) h])
          (subdivide depth [(+ x (/ w 2)) y (/ w 2) h]))))))

(defn draw []
  (q/background 0)
  (q/no-stroke)
  (subdivide 0 [0 0 (q/width) (q/height)])
  (when @render
    (q/save "out/subdivide.png")
    (reset! render false))
  (q/no-loop))
