(ns quils.subdivide.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.subdivide.core :as core]))

(q/defsketch subdivide
  :title "Subdivide"
  :size (repeat 2 1080)
  :setup core/setup
  :draw core/draw
  :features [])

(defn refresh []
  (require 'quils.subdivide.core :reload)
  (.loop subdivide))
