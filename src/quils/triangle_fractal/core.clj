(ns quils.triangle-fractal.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def number-of-iterations 100)
(def recursion 4)

(defn setup []
  (q/frame-rate 30)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration #(-> % inc (mod number-of-iterations))))

(defn triangle-props [side-length]
  (let [circumscribed-radius (/ side-length (q/sqrt 3))]
    {:circumscribed-radius circumscribed-radius
     :inscribed-radius (/ circumscribed-radius 2)
     :half-side (/ side-length 2)}))

(defn make-triangle [x y side-length]
  (let [{:keys [circumscribed-radius
                inscribed-radius
                half-side]} (triangle-props side-length)]
  (let [circumscribed-radius (/ side-length (q/sqrt 3))]
    {:circumscribed-radius circumscribed-radius
     :inscribed-radius (/ circumscribed-radius 2)
     :half-side (/ side-length 2)})
    [[x (+ y circumscribed-radius)]
     [(- x half-side) (- y inscribed-radius)]
     [(+ x half-side) (- y inscribed-radius)]]))

(def draw-subdivisions
  (let [iteration-step (/ 255 number-of-iterations)
        depth-step (/ 255 recursion)
        rotate-step (/ u/TAU number-of-iterations)]
    (fn [draw y1 iteration depth]
      (when-not (zero? depth)
        (let [color (mod (+ (* iteration iteration-step)
                            (* (dec depth) depth-step))
                         255)]
          (q/fill color 255 (* 255 (/ (q/mouse-y) (q/height))))
          (q/stroke color 255 (* 255 (/ (q/mouse-x) (q/width)))))
        (q/with-rotation [(* rotate-step iteration)] (draw))
        (doseq [x (range 0 6)]
          (u/with-matrix
            (q/rotate (* x (/ (* 2 q/PI) 6)))
            (q/translate 0 (- (* y1 (/ 2 3))))
            (q/scale (/ 1 3))
            (draw-subdivisions draw y1 iteration (dec depth))))))))

(defn draw-fractal [iteration triangle]
  (let [[[x1 y1] [x2 y2] [x3 y3]] triangle
        draw #(q/triangle x1 y1 x2 y2 x3 y3)]
    (draw-subdivisions draw y1 iteration recursion)))

(defn draw-state [state]
  (q/background 32)
  (q/stroke-weight 1)
  (u/with-matrix
    (let [color (* (/ 255 number-of-iterations)
                   (:iteration state))]
      (q/fill color 255 255)
      (q/stroke color 255 255))
    (q/translate (/ (q/width) 2) (/ (q/height) 2))
    (draw-fractal (:iteration state)
                  (make-triangle 0 0 (* (q/width) (/ 5 6))))))
