(ns quils.triangle-fractal.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.triangle-fractal.core :as core]))

(q/defsketch star-fractal
  :title "Star Fractal"
  :size [500 500]
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
