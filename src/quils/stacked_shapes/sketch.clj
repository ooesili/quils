(ns quils.stacked-shapes.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.stacked-shapes.core :as core]))

(q/defsketch stacked-shapes
  :title "Stacked Shapes"
  :size [600 600]
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
