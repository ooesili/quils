(ns quils.stacked-shapes.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def render false)
(def num-frames 5)

(defn setup []
  (q/frame-rate 8)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(def triangle-points
  [[-19 0]
   [11 -3]
   [-7 14]])

(defn shift-points [points x-shift y-shift]
  (for [[x y] points]
    [(+ x x-shift)
     (+ y y-shift)]))

(defn draw-triangle [points]
  (apply q/triangle (apply concat points)))

(defn draw-shapes [state]
  (q/background 0 0 0)
  (q/fill 0 0 255)
  (q/stroke-weight 0.4)
  (q/translate 50 50)
  (q/scale 12)
  (let [shifted-triangle (shift-points triangle-points 4 -4)
        max-depth 100]
    (doseq [depth (range 1 (+ 1 max-depth))]
      (let [main-color (- 255 (* 255 (/ depth max-depth)))]
        (draw-triangle shifted-triangle)
        (q/rotate (* u/TAU (/ 1 80)))
        (if (zero? (mod (+ (:iteration state) depth) 5))
          (do
            (q/fill 0 0 0)
            (q/stroke 0 0 0))
          (do
            (q/fill 0 0 main-color)
            (q/stroke 0 0 main-color)))
        (q/scale 0.95)))))

(defn draw-state [state]
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))
  (draw-shapes state)
  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/stacked-shapes/frame-%03d.tif" iteration))
        (q/exit)))))
