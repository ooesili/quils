(ns quils.light-waves.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.light-waves.core :as core]))

(q/defsketch light-waves
  :title "Light Waves"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
