(ns quils.light-waves.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def random-seed 3)
(def render false)

(defn setup []
  (q/frame-rate 1)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn draw-wave [state freq amp]
  (let [iterations 50
        radius 1
        xmax (q/sqrt (* 2 (q/sq 50)))
        xstep (/ iterations xmax)]
    (q/begin-shape)
    (doseq [i (range (- iterations) iterations)]
      (let [x (* xmax (/ i iterations))
            y (* amp (q/sin (* freq x)))]
        (q/ellipse x y radius radius)
        (q/curve-vertex x y)))
    (q/end-shape)))

(defn draw-state [state]
  (q/background 0 0 0)
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor)
    (q/stroke-weight 0.15))

  (q/no-fill)

  (q/random-seed random-seed)
  (let [ymax (q/sqrt (* 2 (q/sq 50)))]
    (dotimes [_ 5000]
      (let [angle (q/random 1)
            hue (mod (* 2 256 angle) 256)
            radius (q/random 16)
            y (q/random 10 ymax)]
        (q/with-rotation [(* angle u/TAU)]
          (q/stroke hue 255 255 (* 40 (/ y ymax)))
          (q/ellipse 0 y radius radius)))))

  (let [waves 25
        rays 1]
    (doseq [wave (range 1 (inc waves))]
      (doseq [ray (range rays)]
        (q/with-rotation [(+ (* 1/8 u/TAU) (* ray 1/2 (/ 1 rays) u/TAU))]
          (let [hue (mod (* 256 (/ (- wave 2) waves)) 256)]
            (q/stroke hue 255 255 128)
            (draw-wave state (* 1/8 (/ wave waves)) (* wave 1)))))))

  (when render
    (q/save "out/light-waves.png")
    (q/exit)))
