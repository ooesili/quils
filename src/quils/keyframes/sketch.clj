(ns quils.keyframes.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.keyframes.core :as core]))

(q/defsketch keyframes
  :title "keyframes"
  :size (repeat 2 640)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
