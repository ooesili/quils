(ns quils.keyframes.core
  (:require [quil.core :as q]
            [quils.util :as u]))


(def duration-seconds 5)
(def frame-rate-seconds 60)
(def render true)
(def num-frames (* frame-rate-seconds duration-seconds))

(defn setup []
  (q/frame-rate frame-rate-seconds)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn state->frame [state]
  (mod (:iteration state) num-frames))

(defprotocol Component
  (render-component [this uv]))

(defrecord Ball [x1 y1 x2 y2]
  Component
  (render-component [_ uv]
    (q/no-stroke)
    (q/fill 0)
    (let [radius 10]
      (cond
        (< uv 0) (u/circle x1 y1 radius)
        (< 1 uv) (u/circle x2 y2 radius)
        :else (u/circle
               (q/map-range uv 0 1 x1 x2)
               (q/map-range uv 0 1 y1 y2)
               radius)))))

(defn draw-state [state]
  (q/background 200)
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (let [animation [{:component (->Ball -30 -30 -30 30)
                    :timing [0 0.9]}
                   {:component (->Ball 0 -30 0 30)
                    :timing [0.2 0.8]}
                   {:component (->Ball 30 -30 30 30)
                    :timing [0.3 0.7]}]]
    (let [global-uv (/ (state->frame state) num-frames)]
      (doseq [{:keys [component] [begin end] :timing} animation]
        (let [component-uv (q/map-range global-uv begin end 0 1)]
          (render-component component component-uv)))))

  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/keyframes/frame-%03d.tif" iteration))
        (q/exit)))))
