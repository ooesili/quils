(ns quils.util
  (:refer-clojure :exclude [shuffle])
  (:require [quil.core :as q]))

(defmacro with-matrix [& body]
  `(do
     (q/push-matrix)
     (try
       ~@body
       (finally (q/pop-matrix)))))

(defmacro with-style [& body]
  `(do
     (q/push-style)
     (try
       ~@body
       (finally (q/pop-style)))))

(def TAU (* 2 q/PI))

(def PHI 1.61803398875)

(deftype Complex [real imag])

(defn cx+ [^Complex z1 ^Complex z2]
  (let [x1 (double (.real z1))
        y1 (double (.imag z1))
        x2 (double (.real z2))
        y2 (double (.imag z2))]
    (Complex. (+ x1 x2) (+ y1 y2))))

(defn cx* [^Complex z1 ^Complex z2]
  (let [x1 (double (.real z1))
        y1 (double (.imag z1))
        x2 (double (.real z2))
        y2 (double (.imag z2))]
    (Complex. (- (* x1 x2) (* y1 y2)) (+ (* x1 y2) (* y1 x2)))))

(defn rotate-3d [rotation point]
  (let [[x y z] point
        [rx ry rz] rotation
        cx (q/cos rx)
        cy (q/cos ry)
        cz (q/cos rz)
        sx (q/sin rx)
        sy (q/sin ry)
        sz (q/sin rz)
        dx (- (* cy (+ (* sz y)
                       (* cz x)))
              (* sy z))
        dy (+ (* sx (+ (* cy z)
                       (* sy (+ (* sz y)
                                (* cz x)))))
              (* cx (- (* cz y)
                       (* sz x))))
        dz (- (* cx (+ (* cy z)
                       (* sy (+ (* sz y)
                                (* cz x)))))
              (* sx (- (* cz y)
                       (* sz x))))]
    [dx dy dz]))

(defn translate-3d [translation point]
  (map + translation point))

(defn- project-3d
  [object camera-position camera-angle display-surface]
  (let [$x (fn [[x _ _]] x)
        $y (fn [[_ y _]] y)
        $z (fn [[_ _ z]] z)
        x (- ($x object) ($x camera-position))
        y (- ($y object) ($y camera-position))
        z (- ($z object) ($z camera-position))
        [dx dy dz] (rotate-3d camera-angle [x y z])
        bx (+ (* (/ ($z display-surface) dz)
                 dx)
              ($x display-surface))
        by (+ (* (/ ($z display-surface) dz)
                 dy)
              ($y display-surface))]
    [bx by]))

(defn new-camera [position angle zoom]
  {:position position
   :angle angle
   :zoom zoom})

(defn camera-project [camera object-position]
  "Project an object from 3D space into 2D space using a camera "
  (let [{:keys [position angle zoom]} camera]
    (project-3d object-position
                position
                angle
                [0 0 zoom])))

(defn camera-projected-size [camera position distance]
  (let [other-position (->> [distance 0 0]
                            (rotate-3d (:angle camera))
                            (translate-3d position))
        [x1 y1] (camera-project camera position)
        [x2 y2] (camera-project camera other-position)]
    (q/dist x1 y1 x2 y2)))

(defn circle [x y radius]
  (let [diamater (* 2 radius)]
    (q/ellipse x y diamater diamater)))

(defn norm-range
  "Creates a normalized range of evenly spaced numbers of the given size."
  [size]
  (range 0 1 (/ 1.0 size)))

(defn denorm
  "Maps a normalized number to a range of numbers."
  [val start stop]
  (q/map-range val 0 1 start stop))

(defn norm-random
  "Returns a random floating point number between 0 and 1."
  []
  (q/random 0.0 1.0))

(defn hsl
  "Turns an HSL color into an RGB color."
  [hue saturation lightness]
  {:pre [(<= 0 saturation 1)
         (<= 0 lightness 1)]}
  (let [hue (mod hue 1)
        q (if (< lightness 0.5)
            (* lightness
               (+ 1 saturation))
            (- (+ lightness saturation)
               (* saturation lightness)))
        p (- (* 2 lightness) q)
        hue-to-rgb (fn [p q hue]
                     (let [hue (cond
                               (< hue 0) (inc hue)
                               (< 1 hue) (dec hue)
                               :else hue)]
                       (* (cond
                            (< (* 6 hue) 1) (+ p (* (- q p) hue 6))
                            (< (* 2 hue) 1) q
                            (< (* 3 hue) 2) (+ p (* (- q p) 6 (- 2/3 hue)))
                            :else         p)
                          256)))]
    [(max 0 (hue-to-rgb p q (+ hue 1/3)))
     (max 0 (hue-to-rgb p q hue))
     (max 0 (hue-to-rgb p q (- hue 1/3)))]))

(defn split-colors
  "Returns an n-length lazy sequence of RGB values that add up to white when
  blended with additive blending."
  [n]
  (let [saturation 1
        lightness (/ 1 n)]
    (reverse
     (for [hue (range 0 1 (/ 1 n))]
       (hsl hue saturation lightness)))))

(defn shuffle
  "Same as clojure.core/shuffle but uses quil's random number generator."
  [^java.util.Collection items]
  (->> items
       (map vector (repeatedly norm-random))
       (sort-by first)
       (map second)))
