(ns quils.genuary.jan11.core
  (:require [quil.core :as q]
            [quils.util :as u]
            [cheshire.core :as json]
            [clojure.java.io :as io]))

(def duration-seconds 10)
(def frame-rate-seconds 60)
(def render false)
(def num-frames (* frame-rate-seconds duration-seconds))

(defonce sensor-data
  (with-open [file (io/reader (System/getenv "SENSOR_DATA_FILE"))]
    (doall (map #(json/parse-string % true) (line-seq file)))))

(def gyroscope-readings
  (filter (comp #{"Gyroscope"} :sensor) sensor-data))

(defn setup []
  (q/frame-rate frame-rate-seconds)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn progress [state]
  (let [frame (mod (:iteration state) num-frames)]
    (q/norm frame 0 num-frames)))

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/background 96 64 20)
  (q/no-stroke)

  (q/rotate (* 1/4 u/TAU))
  (let [rotation [(q/lerp 0 u/TAU (progress state)) 0 0]
        camera-position (u/rotate-3d rotation [0 0 -23])
        camera-angle (let [[x y z] rotation]
                       [x (+ y (* 1/2 u/TAU)) z])
        camera-zoom 80
        camera (u/new-camera camera-position
                             camera-angle
                             camera-zoom)
        [cx cy cz] camera-position
        objects (->> gyroscope-readings
                     (map-indexed vector)
                     (pmap (fn [[i reading]]
                             (let [position ((juxt :x :y :z) reading)
                                   [x y] (u/camera-project camera position)
                                   radius (u/camera-projected-size camera position 0.2)]
                               {:x x
                                :y y
                                :radius radius
                                :hue (mod (+ (q/map-range i 0 (count gyroscope-readings) 0 256)
                                             (q/lerp 0 256 (progress state)))
                                          256)})))
                     (sort-by :radius))]
    (doseq [{:keys [x y radius hue]} objects]
      (q/fill hue 224 224 144)
      (u/circle x y radius)))

  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/genuary.jan11/frame-%03d.tif" iteration))
        (q/exit)))))
