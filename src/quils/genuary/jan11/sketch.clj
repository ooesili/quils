(ns quils.genuary.jan11.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan11.core :as core]))

(q/defsketch genuary.jan11
  :title "genuary.jan11"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
