(ns quils.genuary.jan03.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan03.core :as core]))

(q/defsketch genuary.jan03
  :title "genuary.jan03"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
