(ns quils.genuary.jan03.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def render false)

(defn setup []
  (q/frame-rate 1)
  (q/color-mode :rgb)
  {})

(defn update-state [state]
  state)

(defn deform-polygon [variance polygon]
  (let [sides (partition 2 1 [(first polygon)] polygon)]
    (mapcat
     (fn [[[x1 y1] [x2 y2]]]
       (let [midpoint-x (/ (+ x1 x2) 2)
             midpoint-y (/ (+ y1 y2) 2)
             new-x (+ midpoint-x (* variance (q/random-gaussian)))
             new-y (+ midpoint-y (* variance (q/random-gaussian)))]
         [[x1 y1] [new-x new-y]]))
     sides)))

(defn draw-polygon [polygon]
  (q/begin-shape)
  (doseq [[x y] polygon] (q/vertex x y))
  (q/end-shape :close))

(defn render-glyph []
  (let [num-strokes (q/random 2 5)]
    (dotimes [_ num-strokes]
      (let [num-points (int (q/random 4 7))
            points (repeatedly
                    num-points
                    (fn [] (let [radius (q/sqrt (q/random 0 1))
                                 angle (q/random 0 u/TAU)
                                 x (* (* radius 0.5) (q/cos angle))
                                 y (* radius (q/sin angle))]
                             [x y])))]

        (doseq [replica (u/norm-range 30)]
          (let [magnitude 0.05
                offset (u/denorm replica (- magnitude) magnitude)]
            (q/begin-shape)
            (doseq [[x y] points]
              (q/curve-vertex (+ offset x) (+ offset y)))
            (q/end-shape)))))))

(defn iterate-times [times f x]
  (nth (iterate f x) times))

(defn draw-watercolor [splotch]
  (let [{:keys [polygon color]} splotch
        num-layers 7
        alpha (/ 255 num-layers)]
    (u/with-matrix
      (q/scale (q/width) (q/height))
      (u/with-style
        (q/no-stroke)
        (apply q/fill (concat color [alpha]))
        (dotimes [_ num-layers]
          (->> polygon
               (iterate-times 5 (partial deform-polygon 0.03))
               (iterate-times 7 (partial deform-polygon 0.01))
               draw-polygon))))))

(defn draw-state [state]
  (q/background 0xe0 0xd3 0xaf)
  (q/no-fill)
  (q/stroke-weight 0.02)

  (q/random-seed 54)

  (doseq [splotch
          [{:polygon [[0.7 1]
                      [1 1]
                      [1 0.3]
                      [1 0.3]]
            :color [0xd0 0xbc 0x85]}
           {:polygon [[0 0]
                      [0 0.8]
                      [0.4 0.8]
                      [0.8 0]]
            :color [0xd0 0xbc 0x85]}
           {:polygon [[0 0]
                      [0 0.4]
                      [0.2 0.4]
                      [0.5 0]]
            :color [0xc8 0xb1 0x71]}
           {:polygon [[0.6 1]
                      [0 1]
                      [0.5 0.5]]
            :color [0xc8 0xb1 0x71]}
           {:polygon [[0.2 0.8]
                      [0.2 0.6]
                      [0.6 0.6]
                      [0.6 0.2]]
            :color [0xe2 0xd6 0xb4]}
           {:polygon [[0 0]
                      [0 0.2]
                      [0.2 0.1]]
            :color [0xe2 0xd6 0xb4]}]]
    (draw-watercolor splotch))

  (let [total-size 10
        glyph-size (/ 1 total-size)
        scale-x (u/denorm (* 0.45 glyph-size) 0 (q/width))
        scale-y (u/denorm (* 0.45 glyph-size) 0 (q/height))]
    (doseq [x (range 0 1 glyph-size)
            y (range 0 1 glyph-size)]
      (u/with-matrix
        (q/translate
         (u/denorm (+ x (* 0.5 glyph-size)) 0 (q/width))
         (u/denorm (+ y (* 0.5 glyph-size)) 0 (q/height)))
        (q/scale scale-x scale-y)
        (if (< 0.08 (u/norm-random))
          (q/stroke 0x2a 0x28 0x21)
          (q/stroke 0xd0 0x40 0x00))
        (render-glyph))))

  (when render
    (q/save "out/genuary.jan03.png")
    (q/exit)))
