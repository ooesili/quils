(ns quils.genuary.jan06.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan06.core :as core]))

(q/defsketch genuary.jan06
  :title "genuary.jan06"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
