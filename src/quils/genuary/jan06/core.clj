(ns quils.genuary.jan06.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def render false)

(defn setup []
  (q/frame-rate 1)
  (q/color-mode :hsb)
  nil)

(defn update-state [state]
  state)

(defn draw-polygon [polygon]
  (q/begin-shape)
  (doseq [[x y] polygon] (q/vertex x y))
  (q/end-shape :close))

(defn subdivide [triangle]
  (let [point (first triangle)
        [[x1 y1] [x2 y2]] (rest triangle)
        i (q/constrain (+ 0.5 (* 0.5 (q/random-gaussian))) 0 1)
        new-point [(q/lerp x1 x2 i)
                   (q/lerp y1 y2 i)]]
    [triangle
     [[x1 y1] point new-point]]))

(defn iterate-times [times f x]
 (nth (iterate f x) times))

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/random-seed 7)

  (q/background 212)
  (q/no-stroke)

  (doseq [seed-triangle [[[50 50] [-50 50] [0 -50]]
                         [[50 50] [50 -50] [10 -50]]
                         [[-50 50] [-50 -50] [-10 -50]]]]
    (let [triangles (iterate-times 11 (partial mapcat subdivide) [seed-triangle])]
      (doseq [triangle triangles]
        (q/fill (q/random 0 256) 255 255 36)
        (draw-polygon triangle))))

  (when render
    (q/save "out/genuary.jan06.png")
    (q/exit)))
