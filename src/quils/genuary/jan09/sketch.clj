(ns quils.genuary.jan09.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan09.core :as core]))

(q/defsketch genuary.jan09
  :title "genuary.jan09"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
