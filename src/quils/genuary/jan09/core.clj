(ns quils.genuary.jan09.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def duration-seconds 5)
(def frame-rate-seconds 60)
(def render false)
(def num-frames (* frame-rate-seconds duration-seconds))

(defn setup []
  (q/frame-rate frame-rate-seconds)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn progress [state]
  (let [frame (mod (:iteration state) num-frames)]
    (q/norm frame 0 num-frames)))

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/background 224)

  (q/no-fill)
  (q/stroke-weight 0.6)
  (q/stroke-cap :square)
  (q/stroke 0)
  (let [radius 1]
    (doseq [i (u/norm-range 100)]
      (doseq [j (u/norm-range 100)]
        (let [center-x (+ radius (q/lerp -50 50 j))
              center-y (+ (q/lerp -60 60 i))
              angle-1 (* (q/sq (+ 50 i))
                         (q/lerp 0 u/TAU j))
              angle-2 (+ angle-1 (* 1/2 u/TAU))]
          (q/line
           (+ center-x (* radius (q/cos angle-1)))
           (+ center-y (* radius (q/sin angle-1)))
           (+ center-x (* radius (q/cos angle-2)))
           (+ center-y (* radius (q/sin angle-2))))))))

  (q/no-stroke)
  (q/fill 0)
  (let [loops 5]
    (doseq [i (u/norm-range (+ 100 loops))]
      (let [x (+ (q/lerp (- -50 loops) 50 i)
                 (q/lerp 0 loops (progress state)))]
        (q/rect x -50 0.75 100))))

  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/genuary.jan09/frame-%03d.tif" iteration))
        (q/exit)))))
