(ns quils.genuary.jan10.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def render false)

(defn setup []
  (q/frame-rate 1)
  (q/color-mode :hsb)
  nil)

(defn update-state [state]
  state)

(defn draw-branch [x y depth angle length]
  (let [x2 (+ x (* length (q/cos angle)))
        y2 (+ y (* length (q/sin angle)))]
    (q/line x y x2 y2)
    (when (< 1 depth)
      (doseq [angle [(+ angle (* 1/16 u/TAU))
                     (- angle (* 1/16 u/TAU))]]
        (draw-branch x2 y2 (dec depth) angle length)))))

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/background 224)

  (q/stroke-cap :square)
  (q/stroke 128 80 64 64)
  (q/stroke-weight 0.17)
  (draw-branch 0 -40 20 (* 1/4 u/TAU) 8)

  (when render
    (q/save "out/genuary.jan10.png")
    (q/exit)))
