(ns quils.genuary.jan10.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan10.core :as core]))

(q/defsketch genuary.jan10
  :title "genuary.jan10"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
