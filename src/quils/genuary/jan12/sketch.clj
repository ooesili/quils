(ns quils.genuary.jan12.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan12.core :as core]))

(q/defsketch genuary.jan12
  :title "genuary.jan12"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
