(ns quils.genuary.jan12.core
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [quil.core :as q]
            [quils.util :as u]))

(def render false)

(defn find-index [v coll]
  (first (keep-indexed (fn [index elem] (when (= elem v) index)) coll)))

(def daily-cumulative-vaccines
  (with-open [file (io/reader (System/getenv "CHICAGO_COVID_VACCINE_CSV"))]
    (let [rows (csv/read-csv file)
          header (first rows)
          column-number (find-index "Total Doses - Cumulative" header)
          vaccines-since-dec-25th-2020 (map #(Integer. (nth % column-number)) (rest rows))
          days-with-no-vaccines 326]
      (doall (concat (repeat days-with-no-vaccines 0)
                     vaccines-since-dec-25th-2020)))))

(defn setup []
  (q/frame-rate 1)
  (q/color-mode :hsb)
  nil)

(defn update-state [state]
  state)

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/random-seed 12)

  (q/background 164 64 78)
  (q/stroke-weight 0.22)

  (let [max-vaccines (reduce max daily-cumulative-vaccines)
        num-data-points (count daily-cumulative-vaccines)
        camera-position [0 1 -25]
        camera-angle [0 0 0]
        camera-zoom 52
        camera (u/new-camera camera-position
                             camera-angle
                             camera-zoom)
        objects (->> (map-indexed vector daily-cumulative-vaccines)
                     (map (fn [[index vaccines]]
                            (let [radius (q/map-range index 0 num-data-points 18 0)
                                  rot1 (q/random 0 u/TAU)
                                  rot2 (q/random 0 (* 1/2 u/TAU))
                                  position (->> [0 0 0]
                                                (u/translate-3d [radius 0 0])
                                                (u/rotate-3d [rot1 rot2 0]))
                                  size (q/map-range vaccines 0 max-vaccines 0.2 17)
                                  projected-size (u/camera-projected-size camera position size)
                                  [x y] (u/camera-project camera position)]
                              {:position position
                               :size size
                               :color [128
                                       4
                                       (q/map-range vaccines 0 max-vaccines 0 256)
                                       192]})))
                     (sort-by (fn [{:keys [position]}]
                                (- (apply q/dist (concat camera-position position))))))]
    (doseq [{:keys [color position size]} objects]
      (let [projected-size (u/camera-projected-size camera position size)
            [x y] (u/camera-project camera position)
            angle (q/random 0 u/TAU)
            outer-ring-size (* 2 projected-size)]
        (q/no-stroke)
        (apply q/fill color)
        (u/circle x y projected-size)

        (q/no-fill)
        (apply q/stroke color)
        (q/line
         x
         y
         (+ x (* outer-ring-size (q/cos angle)))
         (+ y (* outer-ring-size (q/sin angle))))
        (u/circle x y outer-ring-size))))

  (when render
    (q/save "out/genuary.jan12.png")
    (q/exit)))
