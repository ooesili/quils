(ns quils.genuary.jan08.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def render false)

(defn setup []
  (q/frame-rate 1)
  {:iteration 0})

(defn update-state [state]
  state)

(defn draw-wave [x width velocity]
  (let [phase (q/random 0 u/TAU)]
    (doseq [j (u/norm-range 4000)]
      (let [y (q/lerp 60 -60 j)
            width (q/map-range (+ (q/sin (* velocity (q/lerp 0 (* 10 u/TAU) j)))
                                  (q/sin (* velocity (+ phase (q/lerp 0 (* 3 u/PHI u/TAU) j)))))
                               -2 2
                               1 width)
            height 11]
        (q/arc x y width height 0 (* 1/2 u/TAU))))))

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/random-seed 8)

  (q/no-fill)
  (q/stroke-weight 0.1)
  (q/background 32)

  (let [waves [{:color [0 32] :x 0 :width 150 :velocity 1/6}
               {:color [64 32] :x 0 :width 100 :velocity 1/3}
               {:color [0x97 0xcc 0x04 64] :x -30 :width 30 :velocity 1}
               {:color [0xee 0xb9 0x02 64] :x 0 :width 30 :velocity 1}
               {:color [0xf4 0x5d 0x01 64] :x 30 :width 30 :velocity 1}]]
    (doseq [{:keys [color width x velocity]} waves]
      (apply q/stroke color)
      (draw-wave x width velocity)))

  (when render
    (q/save "out/genuary.jan08.png")
    (q/exit)))
