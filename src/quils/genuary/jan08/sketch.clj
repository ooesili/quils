(ns quils.genuary.jan08.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan08.core :as core]))

(q/defsketch genuary.jan08
  :title "genuary.jan08"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
