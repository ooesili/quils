(ns quils.genuary.jan05.sketch
  (:require [quil.core :as q]))

(defn draw-state []
  (q/random-seed 5)
  (q/background 220 30 30)
  (q/stroke-weight (/ (q/width) 750))
  (dotimes [_ 100000]
    (let [radius (* (q/width) 0.001 (q/sq (+ 1 (* 9 (q/random-gaussian)))))
          angle (q/random 0 (* 2 q/PI))]
      (q/line (+ (/ (q/width) 2) (* radius (q/cos angle)))
              (+ (/ (q/height) 2) (* radius (q/sin angle)))
              (+ (/ (q/width) 2) (* 0.9 radius (q/cos (+ angle 0.1))))
              (+ (/ (q/height) 2) (* radius (q/sin (+ angle 0.1))))))))

(q/defsketch genuary.jan05
  :title "genuary.jan05"
  :size (repeat 2 1080)
  :draw draw-state)
