(ns quils.genuary.jan02.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan02.core :as core]))

(q/defsketch genuary.jan02
  :title "genuary.jan02"
  :size [1080 1080]
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
