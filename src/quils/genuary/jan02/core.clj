(ns quils.genuary.jan02.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def render false)

(defn setup []
  (q/frame-rate 1)
  (q/color-mode :rgb)
  nil)

(defn update-state [state]
  state)

(defn cell [rule]
  {:rule rule})

(defn next-cell [left center right]
  (case (mapv some? [left center right])
    [true  true  true]  nil
    [true  true  false] nil
    [true  false true]  nil
    [true  false false] (cell 0)
    [false true  true]  (cell 1)
    [false true  false] (cell 2)
    [false false true]  (cell 3)
    [false false false] nil))

(defn next-gen [cells]
  (into []
        (map-indexed
         (fn [i cell]
           (next-cell
            (get cells (dec i))
            (get cells i)
            (get cells (inc i)))))
        cells))

(defn initial-cells [num-cells]
  (let [first-half (int (/ num-cells 2))
        second-half (- num-cells first-half)
        seed (vec (concat (repeat first-half nil)
                          [(cell 0)]
                          (repeat second-half nil)))]
    (nth (iterate next-gen seed) 0)))

(defn draw-state [state]
  (q/background 204 220 209)
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/translate -75 40)
  (q/scale 3 3)
  (q/no-stroke)

  (let [num-rows 400
        num-cells (* 2 num-rows)
        generations (->> (iterate next-gen (initial-cells num-cells))
                         (drop 0)
                         (take num-rows))
        colors [[238 99 82]
                [89 205 144]
                [63 167 214]
                [250 192 94]]]
    (doseq [[row-num cells] (map-indexed vector generations)]
      (doseq [[cell-num cell] (map-indexed vector cells)]
        (when cell
          (let [row-norm (q/norm row-num 0 num-rows)
                cell-norm (q/norm cell-num 0 num-cells)
                radius (u/denorm row-norm 0 52)
                angle (+ (* 3/4 u/TAU)
                         (u/denorm cell-norm 0 (* 9 u/TAU)))
                x (* radius (q/cos angle))
                y (* radius (q/sin angle))]
            (apply q/fill (nth colors (:rule cell)))
            (u/circle x y (u/denorm row-norm 0.05 0.17)))))))

  (when render
    (q/save "out/genuary.jan02.tif")
    (q/exit)))
