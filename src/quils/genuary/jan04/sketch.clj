(ns quils.genuary.jan04.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan04.core :as core]))

(q/defsketch genuary.jan04
  :title "genuary.jan04"
  :size (repeat 2 1080)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
