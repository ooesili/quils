(ns quils.genuary.jan04.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def render true)

(defn setup []
  (q/frame-rate 1)
  (q/color-mode :hsb)
  nil)

(defn update-state [state]
  state)

(defn triangle-angle-from-sides
  [adjacent-1 adjacent-2 opposite]
  (let [a adjacent-1
        b adjacent-2
        c opposite]
    (q/acos (/ (- (q/sq c) (q/sq a) (q/sq b))
               (* -2 a b)))))

(defn overlap? [particle-1 particle-2]
  (let [{[x1 y1] :position r1 :radius} particle-1
        {[x2 y2] :position r2 :radius} particle-2]
    (< (q/dist x1 y1 x2 y2)
       (+ r1 r2))))

(defn new-color [particle-1 particle-2]
  (let [[h1 s1 b1] (:color particle-1)
        [_  _  b2] (:color particle-2)
        c (+ 4 (max b1 b2))]
    [h1 s1 c]))

(defn add-particle [solid radius]
  (if-let [new-solid
           (some
            (fn [[particle-1 particle-2]]
              (let [{[x1 y1] :position r1 :radius} particle-1
                    {[x2 y2] :position r2 :radius} particle-2
                    bond-angle (q/atan2 (- y2 y1) (- x2 x1))
                    inner-angle (triangle-angle-from-sides
                                 (+ radius r1)
                                 (+ r1 r2)
                                 (+ radius r2))
                    potential-particles (for [angle [(+ bond-angle inner-angle)
                                                     (- bond-angle inner-angle)]]
                                          (let [x (+ x1 (* (+ radius r1) (q/cos angle)))
                                                y (+ y1 (* (+ radius r1) (q/sin angle)))]
                                            {:position [x y]
                                             :color (new-color particle-1 particle-2)
                                             :radius radius}))
                    all-other-particles (filter (complement #{particle-1 particle-2})
                                                (:particles solid))]
                (some
                 (fn [particle]
                   (when-not (some #(overlap? particle %) all-other-particles)
                     {:particle particle
                      :bonds [[particle particle-1]
                              [particle particle-2]]}))
                 potential-particles)))
            (u/shuffle (:bonds solid)))]
    (-> solid
        (update :particles (partial cons (:particle new-solid)))
        (update :bonds (partial concat (:bonds new-solid))))
    solid))

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/background 0)
  (q/no-stroke)
  (q/random-seed 4)

  (let [color1 [178 148 0]
        color2 [253 173 0]
        color3 [144 150 0]
        particle-1 {:radius 1
                    :position [-20 -25]
                    :color color1}
        particle-2 {:radius 1
                    :position [-22 -25]
                    :color color1}

        particle-3 {:radius 1
                    :position [30 -5]
                    :color color2}
        particle-4 {:radius 1
                    :position [32 -5]
                    :color color2}

        particle-5 {:radius 1
                    :position [-25 32]
                    :color color3}
        particle-6 {:radius 1
                    :position [-27 32]
                    :color color3}
        seed-solid {:particles [particle-1
                                particle-2
                                particle-3
                                particle-4
                                particle-5
                                particle-6]
                    :bonds [[particle-1 particle-2]
                            [particle-3 particle-4]
                            [particle-5 particle-6]]}
        new-particles 6000
        final-solid (loop [i 0
                           solid seed-solid]
                      (if (< i new-particles)
                        (let [radius (if (< 0.1 (u/norm-random))
                                       1
                                       0.8)]
                          (recur (inc i) (add-particle solid radius)))
                        solid))]
    (doseq [{[x y] :position :keys [radius color]} (:particles final-solid)]
      (apply q/fill color)
      (u/circle x y radius)))

  (when render
    (q/save "out/genuary.jan04.png")
    (q/exit)))
