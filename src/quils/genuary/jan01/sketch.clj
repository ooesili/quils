(ns quils.genuary.jan01.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.genuary.jan01.core :as core]))

(q/defsketch genuary-jan01
  :title "genuary/jan01"
  :size [1080 1080]
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
