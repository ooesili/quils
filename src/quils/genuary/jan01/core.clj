(ns quils.genuary.jan01.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def duration-seconds 5)
(def frame-rate-seconds 60)
(def render false)
(def num-frames (* frame-rate-seconds duration-seconds))

(defn setup []
  (q/frame-rate frame-rate-seconds)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn progress [state]
  (let [frame (mod (:iteration state) num-frames)]
    (q/norm frame 0 num-frames)))

(defn generate [state f asteps bsteps csteps]
  (doseq [a (u/norm-range asteps)
          b (u/norm-range bsteps)
          c (u/norm-range csteps)]
    (f state a b c)))

(defn connect [rotation [x1 y1] [x2 y2]]
  (u/with-matrix
    (q/stroke-weight 0.05)
    (q/stroke 255 32)
    (q/no-fill)

    (let [x (/ (+ x1 x2) 2)
          y (/ (+ y1 y2) 2)
          diam (q/dist x1 y1 x2 y2)
          start (q/atan2 (- y2 y1) (- x2 x1))
          stop (+ start (* 1/32 u/TAU))]
      (q/arc x y
             diam diam
             (+ start rotation)
             (+ stop rotation)))))

(defn render-arc [state a b c]
  (let [angle (u/denorm a 0 u/TAU)
        radius (u/denorm b 0 50)
        rotation (u/denorm c 0 u/TAU)
        expansion (u/denorm c 1 2)]
    (connect (+ rotation (u/denorm (progress state) 0 u/TAU))
             [0 0]
             [(* expansion radius (q/cos angle))
              (* expansion radius (q/sin angle))])))

(defn draw-state [state]
  (q/background 0 0 0)
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (generate state render-arc 8 600 9)

  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/genuary/jan01/frame-%03d.tif" iteration))
        (q/exit)))))
