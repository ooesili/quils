(defproject quils "0.1.0-SNAPSHOT"
  :description "My quil sketches"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/data.csv "1.0.0"]
                 [cheshire "5.10.0"]
                 [quil "3.1.0"]]
  :jvm-options ["-XX:-OmitStackTraceInFastThrow"
                "-Xcheck:jni"]
  :profiles {:dev {:plugins [[lein-gen "0.2.2"]]}}
  :generators [[quils-gen/quils-gen "0.1.0-SNAPSHOT"]])
