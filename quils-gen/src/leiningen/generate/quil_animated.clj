(ns leiningen.generate.quil-animated
  (:require [leiningen.generate.templates :refer [renderer ->files]]
            [leiningen.new.templates :refer [name-to-path render-text]]
            [leiningen.core.main :as main]))

(def render (renderer "quil-animated"))

(defn quil-animated
  "Generate an animated quil."
  [project name]
  (let [data {:name name :sanitized (name-to-path name)}]
    (doseq [basename ["core" "sketch"]]
      (let [filename (str "src/quils/{{sanitized}}/" basename ".clj")]
        (main/info (render-text (str "Creating " filename) data))
        (->files data [filename (render (str basename ".clj") data)])))))
