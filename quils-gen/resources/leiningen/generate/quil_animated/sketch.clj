(ns quils.{{name}}.sketch
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [quils.{{name}}.core :as core]))

(q/defsketch {{name}}
  :title "{{name}}"
  :size (repeat 2 1000)
  :setup core/setup
  :update core/update-state
  :draw core/draw-state
  :features []
  :middleware [m/fun-mode])
