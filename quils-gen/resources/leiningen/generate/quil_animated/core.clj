(ns quils.{{name}}.core
  (:require [quil.core :as q]
            [quils.util :as u]))

(def duration-seconds 5)
(def frame-rate-seconds 15)
(def render false)
(def num-frames (* frame-rate-seconds duration-seconds))

(defn setup []
  (q/frame-rate frame-rate-seconds)
  (q/color-mode :hsb)
  {:iteration 0})

(defn update-state [state]
  (update state :iteration inc))

(defn progress [state]
  (let [frame (mod (:iteration state) num-frames)]
    (q/norm frame 0 num-frames)))

(defn draw-state [state]
  (q/translate (/ (q/width) 2) (/ (q/height) 2))
  (let [scale-factor (/ (min (q/width) (q/height)) 100)]
    (q/scale scale-factor scale-factor))

  (q/background 0)

  (when render
    (let [{:keys [iteration]} state]
      (if (< iteration num-frames)
        (q/save (format "out/{{name}}/frame-%03d.tif" iteration))
        (q/exit)))))
